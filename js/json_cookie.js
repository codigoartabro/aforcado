/**
 * JSON Cookie JS
 * =============================================================================
 *
 * @file json_cookie.js
 * @version v0.0.1
 * @license GNU GPL License
 *
 */

/*
 * This file is part of Aforcado game.
 *
 * Aforcado game is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Aforcado game is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Aforcado game. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Clase JSONCookie
 * 
 * @property {string/json} json
 * 
 * @method getCookie
 * @method setCookie
 */

function JSONCookie()
{
    this.json = "";

    /**
     * Obter contido en JSON dende a cookie
     * a partir do nome da mesma.
     * 
     * @param {string} nome 
     * @returns {json/string}
     */
    this.getCookie = function(nome)
    {
        var nome = nome + "=";
        var inicio;
        var fin;
        var string;

        if(document.cookie.length>0)
        {
            inicio = document.cookie.indexOf(nome);
            if(inicio != -1)
            {
                inicio = inicio + (nome.length);
                fin = document.cookie.indexOf(";",inicio);
                if(fin == -1)
                {
                    fin = document.cookie.length;
                }
                string = document.cookie.substring(inicio, fin);
                this.json = JSON.parse(string);
                return this.json;
            }
        }
        return "";
    };

    /**
     * Crear ou renovar a cookie a partir
     * do string de texto obtido dun array
     * de elementos JSON
     * 
     * @param {array} mellores 
     */
    this.setCookie = function(array)
    {
        var date = new Date();

        this.json = JSON.stringify(array);
		date.setTime(date.getTime()+(365*24*60*60*1000));
        
        document.cookie = "mellores=" + this.json + ";expires=" + date.toGMTString() + ";samesite=lax;";
    }
}