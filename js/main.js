/**
 * Main JS
 * =============================================================================
 *
 * @file main.js
 * @version v0.0.2
 * @license GNU GPL License
 *
 */

/*
 * This file is part of Aforcado game.
 *
 * Aforcado game is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Aforcado game is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Aforcado game. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Obxecto JSONCookie
 */
var cookie = new JSONCookie();

var palabra = "";

/**
 * Array de obxectos Xogador
 */
var mellores = new Array();
var inicio;
var fin;

var puntuacions = document.getElementById("puntuacions");
var taboleiro = document.getElementById("taboleiro");
var imagenes = document.getElementById("imagenes");
var adivinado = document.getElementById("adivinado");

/**
 * Clase Xogador
 * 
 * @param {string} nome 
 * @param {string} puntuacion 
 */
function Xogador(nome, puntuacion)
{
    this.nome = nome;
    this.puntuacion = puntuacion;
}

function Aforcado()
{
    this.diccionario = [
        "abad", "baya", "cafe", "daga", "edil", "faca", "gafa", "haba", "idea",
        "jaca", "leño", "maiz", "nabo", "obra", "paga", "quid", "raiz", "saco",
        "taba", "ubre", "vaca", "yate", "zeta", "abacos", "babada", "cabala",
        "dadiva", "ebrios", "fabada", "gabela", "habano", "ibices", "jabato",
        "lacayo", "maceta", "nabiza", "objeto", "pabilo", "quasar", "rabano",
        "sabado", "tabano", "ubicua", "vacias", "xecudo", "yantas", "zafado",
        "abadejos", "caballos", "dactilar", "ebanista", "fabulosa", "gabinete",
        "habanero", "ibericas", "jabalies", "labiales", "macabros", "nacional",
        "obcecaba", "pabellon", "quebrada", "rabanero", "sabatica", "tabardos",
        "ubicadas", "vacacion", "xenofobo", "yaciendo", "zabucado"
    ];

    this.setPalabra = function()
    {
        var texto;

        console.log("Iniciando Palabra.");
        
        return this.diccionario[Math.floor((Math.random() * this.diccionario.length))];
    }

    this.palabra = this.setPalabra();
    this.errores = 0;
    this.aciertos = 0;
    console.log("Palabra: " + this.palabra);

    this.setLetra = function(letra)
    {
        var adivinada = false;
        var boton = document.getElementById("letra_" + letra);

        console.log("Chequeando Letra: " + letra + ".");

        for(i=0; i < this.palabra.length; i++)
        {
            if(letra == this.palabra[i])
            {
                var char = document.getElementById("char" + i);
                char.innerHTML = letra;
                this.aciertos++;
                adivinada = true;
                boton.classList.add("valida");
                this.setAcierto();
            }
        }
        if(adivinada == false)
        {
            this.errores++;
            console.log("Errores: " + this.errores);
            boton.classList.add("error");
            this.setErrores();
        }
        boton.disabled = true;
    }

    this.setGameOver = function()
    {        
        var botones = document.querySelectorAll("input.letra");
        var field = taboleiro.querySelector("fieldset legend");

        for(i=0; i<botones.length; i++)
        {
            botones[i].disabled = true;
        }

        field.innerHTML = "Game Over.";
        for(i=0; i < this.palabra.length; i++)
        {
            var char = document.getElementById("char" + i);
            char.innerHTML = this.palabra[i];
        }
    }

    this.setWin = function()
    {
        var field = taboleiro.querySelector("fieldset");
        var puntos;
        var tiempo;
        var texto;

        field.innerHTML = "<legend>Palabra Adiviñada.</legend>";
    
        tiempo = fin.getTime() - inicio.getTime();
        console.log(tiempo);
        puntos = ((this.aciertos * 1000000)/this.palabra.length) - (this.errores * 1000) - tiempo;
        console.log(puntos);

        texto = "<p>Aciertos: " + this.aciertos + " = " + ((this.aciertos * 1000000)/this.palabra.length) + "pts.</br>";
        texto = texto + "Errores : " + this.errores + " = -" + (this.errores * 1000) + "pts.</br>";
        texto = texto + "Tiempo  : " + (tiempo / 1000) + "seg = -" + tiempo + "pts.</br>";
        texto = texto + "Total  : " + puntos + "pts.</p>";

        field.innerHTML = field.innerHTML + texto;
    }

    this.setAcierto = function()
    {
        if(this.aciertos == this.palabra.length)
        {
            fin = new Date();
            this.setWin();
        }
    }

    this.setErrores = function()
    {
        imagenes.style.background = "url(img/"+ this.errores + ".png)";
        if(this.errores == 7)
        {
            this.setGameOver();
        }
    }
}

/**
 * Crear Mellores Puntuacions.
 * 
 * Se o primer parametro é verdadeiro
 * introducimos 10 entradas na lista cos 
 * valores por defecto.
 *
 * @param {boolean} all 
 * @param {string} nome 
 * @param {number} puntuacion 
 */

function setMellores(nome="", puntuacion=0)
{
    mellores = new Array();
    for(i=0; i <10; i++)
    {
        /*
            * Engadimos 0s o inicio da puntuacion ata
            * un minimo de 5 dixitos.
            */
        mellores[i] = new Xogador("aaa", "0".padStart(5, "0"));
    }
}

/**
 * Obter a lista dos mellores xogadores.
 */

function getMellores()
{
    var texto;
    var field = puntuacions.querySelector("fieldset");

    /*
     * Cargamos o array de mellores dende a cookie
     */
    mellores = cookie.getCookie("mellores");
    puntuacions.classList.add("activo");
    taboleiro.classList.remove("activo");

    /*
     * Se non hay cookie, o array é valeiro.
     * Creamos unha lista de novos mellores.
     * Gardamos a nova lista na cookie.
     */
    if(mellores == "")
    {
        console.log("Creando Puntuacions Inicias.");
        setMellores(true);
        cookie.setCookie(mellores);
    }
    else
    {
        console.log("Cargando Mellores Puntuacions.");
    }
    
    /*
     * Pintamos a lista dos mellores.
     */
    texto = "<ol>";
    for(xogador of mellores)
    {
        texto = texto + "<li>" + xogador.nome + "<span>" + xogador.puntuacion + "</span></li>";
    }
    texto = texto + "</ol>";
    field.innerHTML = texto + field.innerHTML;

    /*
     * Gardamos a lista na cookie.
     */
    cookie.setCookie(mellores);
}

function startGame()
{
    var texto = "";
    /*var abc = new Array();*/

    console.log("Iniciando novo xogo.");
    
    puntuacions.classList.remove("activo");
    taboleiro.classList.add("activo");

    aforcado = new Aforcado();

    for(i = 0; i < 26; i++)
    {
        var letra = (i+10).toString(36);
        texto = texto + "<input id=\"letra_" + letra + "\" class=\"letra\" type=\"button\" value=\"" + letra + "\" onclick=\"aforcado.setLetra('" + letra + "');\">";
        /*abc[i] = letra;*/
    }

    /*abc[26] = "ñ";*/
    texto = texto + "<input id=\"letra_&ntilde;\" class=\"letra\" type=\"button\" value=\"&ntilde;\" onclick=\"aforcado.setLetra('&ntilde;');\">";

    letras.innerHTML = letras.innerHTML + texto;

    texto = "";
    for(i = 0; i < aforcado.palabra.length; i++)
    {
        texto = texto + "<span id=\"char"+ i + "\" class=\"letra\">&nbsp;</span>";
    }
    adivinado.innerHTML = adivinado.innerHTML + texto;
    inicio = new Date();
}