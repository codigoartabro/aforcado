CHANGELOG
=========
* v.0.1.0
    * Primeiro lanzamento estable.
* v.0.0.2.20201102
    * Partida nova.
* v.0.0.1.20201031
    * Listado de puntuacions, cargada/gardada en cookie.
    * Engadense os arquivos iniciais.
    * Comezo do proxecto.